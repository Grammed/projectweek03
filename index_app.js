const h1Equiv = document.querySelector('.h1-equiv')
const output_1 = document.querySelector('.output_1');
const output_2 = document.querySelector('.output_2');
const error_output = document.querySelector('.error_output');
const inputVal = document.querySelector('.val');
const mainURL = 'https://pokeapi.co/api/v2/';


window.addEventListener('DOMContentLoaded', (myEvent) => {
  console.log('DOM ready');
  fetch(mainURL).then((myResponseObject) => {
      return myResponseObject.json();
    }).then((json) => {

      console.log(json);
      output_1.innerHTML = '';
      for (const prop in json) {
        if (`${prop}`.startsWith('po')) {
          console.log(`${prop} : ${json[prop]}`);
          const btn = document.createElement('button');
          btn.textContent = prop;
          if (`${prop}` != 'pokedex')
            btn.className = "pokeButtons btn btn-light m-2";
          else
            btn.className = "pokeButtons btn btn-success m-2";
          btn.propURL = json[prop];
          output_1.append(btn);
          btn.addEventListener('click', getData);
        }
      }
    })
    .catch((error) => {
      console.log(error);
      output_1.innerHTML = 'Error on fetch/data handling for mainURL https://pokeapi.co/api/v2/';
    })
})

function getData(eventObj) {
  const eventInitiator = eventObj.target;
  getBtnJSON(eventInitiator.propURL);
}

function getBtnJSON(url) {
  fetch(url)
    .then(btnResponseObject => btnResponseObject.json())
    .then(data => {
      buildBtnJSONPage(data);
    })
    .catch((error) => {
      console.log(error);
      output_1.innerHTML = 'Error on fetch/data handling => retrieve regions. Please press the emergency button to move to the page for the Kanto region.';
    })
}

function buildBtnJSONPage(data) {
  console.log(data);
  output_2.innerHTML = '';
  data.results.forEach(dataElement => {
    const div = document.createElement('div');
    div.textContent = dataElement.name;
    if (div.textContent != 'kanto') {
      div.className = "p-5 kanto item-hl";
      div.flexURL = dataElement.url;
      div.addEventListener('click', showItem);
    } else {
      div.className = "p-5 other bg-success item-hl";
      div.flexURL = 'kanto.html';
      div.addEventListener('click', navigate);
    }
    output_2.append(div);
    console.log(dataElement.name);
  })
}

function navigate(el) {
  const element = el.target;
  console.log(element.flexURL);
  window.open(element.flexURL, "_blank")
}

function showItem(el) {
  const element = el.target;
  console.log(element.flexURL);
  fetch(element.flexURL).then(flexResponseObject => flexResponseObject.json())
    .then((jsonData) => {
      console.log(jsonData);
      for (const property in jsonData) {
        console.log(`${property} : ${jsonData[property]}`);
        console.log(typeof (jsonData[property]));
      }
    })

    .catch((error) => {
      console.log(error);
      output_1.innerHTML = 'Error on fetch/data handling => retrieve region data - note: this only displays on console anyway';
    })
}