const btn = document.querySelector('.searchBtn');
const output_1 = document.querySelector('.output_1');
const output_2 = document.querySelector('.output_2');
const modalHTMLBase = document.querySelector('.modal');
const val = document.querySelector('.val');
val.style.visibility = "hidden";
const orderedPokeData = new Array(151);
const unorderedPokeData = [];
const totalPokes = 151;
let count = 0;
const page = {
  json: {},
  pageNum: 1,
  ElementsPerPage: 151,
  arrayOfPages: []
};

btn.textContent = 'Load Kanto Pokemon';

const baseURL = 'https://pokeapi.co/api/v2/pokedex/';
fetchData()
window.addEventListener('DOMContentLoaded', (myEvent) => {
  console.log('DOM ready');
}) //DomContentLoaded

btn.addEventListener('click', (clickEvent) => {
  console.log('ready');
  const para = '2';
  const url = baseURL + para;
  fetch(url)
    .then(responseObject => responseObject.json())
    .then(data => {
      data = data.pokemon_entries;
      page.json = data;
      createPages(data);
    }).catch((error) => {
      console.log(error);
      output_1.innerhtml = 'error on fetch/data handling for: ' + url;
    })
})

function fetchData() {
  let typeURL = 'https://pokeapi.co/api/v2/pokemon/';
  let count = 0;
  for (let i = 0; i < totalPokes; i++) {
    URLb = typeURL + (i + 1);
    console.log(URLb);
    fetch(URLb)
      .then(response => response.json())
      .then(pokemonJSON => {
        count = (fillArray(pokemonJSON));
        console.log(count);
      }).catch((error) => {
        alert(error);
        console.log(error);
        output_2.innerhtml = 'error @fetchData()  for: ' + URLb;
      })
  }
  return true;
}

function fillArray(element) {
  console.log(element);
  orderedPokeData.splice(element.id - 1, 1, element);
  unorderedPokeData.push(element);
}

function createPages(data) {
  page.arrayOfPages.length = 0;
  for (let i = 0; i < data.length; i += page.ElementsPerPage) {
    let tempPageArray = data.slice(i, i + page.ElementsPerPage);
    page.arrayOfPages.push(tempPageArray);
  }
  loadPages();
}

function loadPages() {
  output_2.innerHTML = '';
  page.arrayOfPages[page.pageNum - 1].forEach(arrayElement => {
    pageElement(arrayElement);
  });
}

async function pageElement(data) {
  await new Promise(r => setTimeout(r, 2000));
  elementNumber = data.entry_number;
  const main = createNode(output_2, 'div', '');
  main.className = "flex_button  bg-white m-2 justify-content-center";
  const title = createNode(main, 'div', `${elementNumber}:  ${data.pokemon_species.name}`);
  title.className = 'row lead smCardLead justify-content-center mt-1 mx-4';
  title.setAttribute('id', `${data.entry_number}`);
  title.addEventListener('click', getData);
  title.setAttribute('data-toggle', 'modal');
  title.setAttribute('data-target', '#modalHTMLBase')
  const picDiv = createNode(main, 'div', '');
  picDiv.className = "m-1 pb-2 row justify-content-center";
  const pic = createNode(picDiv, 'img', '');
  pic.setAttribute('src', "img/" + `${elementNumber}` + '.png');
  const typeRow = createNode(main, 'div', '');
  typeRow.className = "m-1 mb-2 row justify-content-center";
  const typeOne = createNode(typeRow, 'div', `${orderedPokeData[elementNumber - 1].types[0].type.name}`);
  typeOne.className = "type pb-1 px-3 mx-1";
  if (orderedPokeData[elementNumber - 1].types.length === 1) {
    const typeTwo = createNode(typeRow, 'div', '<small>****</small>');
    typeTwo.className = 'type pb-1 px-3 mx-1';
  } else {
    const typeTwo = createNode(typeRow, 'div', `${orderedPokeData[elementNumber - 1].types[1].type.name}`);
    typeTwo.className = "type pb-1 px-3 mx-1";
  }
}

function createNode(parent, elementType, html) {
  const element = document.createElement(elementType);
  parent.append(element);
  element.innerHTML = html;
  return element;
}

function getData(eventObj) {
  const eventInitiator = eventObj.target;
  let buttonData = orderedPokeData[eventInitiator.id - 1];
  console.log(buttonData);
  createModal(buttonData);
}

function createModal(data) {
  if (count > 0)
    modalHTMLBase.innerHTML = '';
  count++;
  const modalDialog = createNode(modalHTMLBase, 'div', '');
  modalDialog.className = "modal-dialog";
  const modalContent = createNode(modalDialog, 'div', '');
  modalContent.className = "modal-content";
  const modalContentHeader = createNode(modalContent, 'div', `${data.id}. ${data.name}`);
  modalContentHeader.className = "modalContentHeader display-4 text-center";
  const modalHeader = createNode(modalContent, 'div', ``);
  modalHeader.className = "modal-header justify-content-between";
  const position01 = createNode(modalHeader, 'div', `<h1>` + `${data.height}` + `</h1><small>HEIGHT</small>`);
  position01.className = "header-item text-left ml-4";
  const position02 = createNode(modalHeader, 'div', `<img src="img/` + `${data.id}` + `.png">`);
  position02.className = "header-item justify-content-center";
  const position03 = createNode(modalHeader, 'div', `<h1>` + `${data.weight}` + `</h1><small>WEIGHT</small>`);
  position03.className = "header-item text-right mr-4";
  const modalDismiss = createNode(modalContent, 'btn', `${'close'}`);
  modalDismiss.className = "close btn btn-light ";
  modalDismiss.setAttribute('data-dismiss', "modal");
  const modalBody = createNode(modalContent, 'div', ``);
  const statCard = createNode(modalBody, 'div', '');
  statCard.className = 'card card-body py2';
  for (let i = 0; i < data.stats.length; i++) {
    const heading = createNode(statCard, 'h5', `${data.stats[i].stat.name}`);
    if (i == 0)
      heading.className = "mt-n3 statHeadingHP";
    else
      heading.className = "mt-n3 statHeading";
    heading.setAttribute("style", "text-left !important")
    const progressDiv = createNode(statCard, 'div', '');
    progressDiv.className = "progress";
    const progressBar = createNode(progressDiv, 'div', '');
    progressBar.className = "progress-bar bg-success";
    progressBar.setAttribute("style", "width:" + `${data.stats[i].base_stat}` + "%");
    const footing = createNode(statCard, 'h5', `${data.stats[i].base_stat}`);
    heading.setAttribute("style", "text-left !important")
    const horizontalRow = createNode(footing, 'hr', '');
  }
}